// cargo doc --no-deps --open
/*!
 * Library for safe math operations, provides functions for working with numbers.
 * Library consists of [LibMath](LibMath) trait implemented for BigDecimal.
 *
 * All functions return either result or Err([CalcError](CalcError)).
 * Result is allways of same type as arguments (e.g. if arguments are i32 and result doesn't fit into i32 but it would fit into i64, [Overflow](CalcError::Overflow) is returned, not i64)
 *
 * # Examples
 *
 * ```
 * use library::LibMath;
 * use bigdecimal::BigDecimal;
 * let i : BigDecimal = 32.into();
 * i.pow(10).unwrap();
 * ```
 * All functions return either resulting variable of same type or [Error](CalcError).
 */

use bigdecimal::{BigDecimal, FromPrimitive, One, ToPrimitive, Zero};

#[cfg(test)]
mod tests {
    use bigdecimal::{BigDecimal, FromPrimitive};

    use crate::{CalcError, LibMath};
    #[test]
    fn check_add() {
        let a: BigDecimal = BigDecimal::from_f32(2.5).unwrap();
        let b: BigDecimal = BigDecimal::from_f32(2.5).unwrap();
        let c: BigDecimal = BigDecimal::from_f32(5.0).unwrap();
        assert_eq!(a.add(b).unwrap(), c);

        let a = BigDecimal::from_f64(f64::MAX).unwrap();
        let b = BigDecimal::from_f64(f64::MAX).unwrap();
        let c = BigDecimal::from_f64(2.0).unwrap();
        let d = BigDecimal::from_f64(f64::MAX).unwrap();
        assert_eq!(a.add(b).unwrap() / c, d);

        let a = BigDecimal::from_f64(f64::MIN).unwrap();
        let b = BigDecimal::from_f64(f64::MIN).unwrap();
        let c = BigDecimal::from_f64(2.0).unwrap();
        let d = BigDecimal::from_f64(f64::MIN).unwrap();
        assert_eq!(a.add(b).unwrap() / c, d);

        let a = BigDecimal::from_f64(f64::MAX).unwrap();
        let b = BigDecimal::from_f64(f64::MIN).unwrap();
        let d = BigDecimal::from_f64(0.0).unwrap();
        assert_eq!(a.add(b).unwrap(), d);

        //try adding 0.1 10 times and comparing it to 1.0, should not fail
        let mut a = BigDecimal::from_f64(0.0).unwrap();
        let c = BigDecimal::from_f64(1.0).unwrap();
        for _ in 0..10 {
            a = a.add(BigDecimal::from_f64(0.1).unwrap()).unwrap();
        }
        assert_eq!(a, c);
    }

    #[test]
    fn check_sub() {
        let a: BigDecimal = BigDecimal::from_f32(2.5).unwrap();
        let b: BigDecimal = BigDecimal::from_f32(2.5).unwrap();
        let c: BigDecimal = BigDecimal::from_f32(0.0).unwrap();
        assert_eq!(a.subtract(b).unwrap(), c);

        let a = BigDecimal::from_f64(f64::MAX).unwrap();
        let b = BigDecimal::from_f64(f64::MAX).unwrap();
        let d = BigDecimal::from_f64(0.0).unwrap();
        assert_eq!(a.subtract(b).unwrap(), d);

        let a = BigDecimal::from_f64(f64::MIN).unwrap();
        let b = BigDecimal::from_f64(f64::MIN).unwrap();
        let d = BigDecimal::from_f64(0.0).unwrap();
        assert_eq!(a.subtract(b).unwrap(), d);

        let a = BigDecimal::from_f64(f64::MIN).unwrap();
        let b = BigDecimal::from_f64(f64::MAX).unwrap();
        let c = BigDecimal::from_f64(2.0).unwrap();
        let d = BigDecimal::from_f64(f64::MIN).unwrap();
        assert_eq!(a.subtract(b).unwrap() / c, d);

        let a = BigDecimal::from_f64(f64::MAX).unwrap();
        let b = BigDecimal::from_f64(f64::MIN).unwrap();
        let c = BigDecimal::from_f64(2.0).unwrap();
        let d = BigDecimal::from_f64(f64::MAX).unwrap();
        assert_eq!(a.subtract(b).unwrap() / c, d);

        // Trying to remove 0.1 from 1.0 10 times, should equal to 0.0
        let mut a = BigDecimal::from_f64(1.0).unwrap();
        let c = BigDecimal::from_f64(0.0).unwrap();
        for _ in 0..10 {
            a = a.subtract(BigDecimal::from_f64(0.1).unwrap()).unwrap();
        }
        assert_eq!(a, c);
    }

    #[test]
    fn check_mult() {
        let a: BigDecimal = BigDecimal::from_f32(10.0).unwrap();
        let b: BigDecimal = BigDecimal::from_f32(10.0).unwrap();
        let c: BigDecimal = BigDecimal::from_f32(100.0).unwrap();
        assert_eq!(a.multiply(b).unwrap(), c);

        let a = BigDecimal::from_f32(10.0).unwrap();
        let b = BigDecimal::from_f32(0.1).unwrap();
        let c = BigDecimal::from_f32(1.0).unwrap();
        assert_eq!(a.multiply(b).unwrap(), c);

        let a = BigDecimal::from_f64(f64::MAX).unwrap();
        let b = BigDecimal::from_f64(0.1).unwrap();
        let c = BigDecimal::from_f64(f64::MAX).unwrap();
        let d = BigDecimal::from_f64(10.0).unwrap();
        assert_eq!(a.multiply(b).unwrap(), c / d);

        let a = BigDecimal::from_f64(f64::MAX).unwrap();
        let b = BigDecimal::from_f64(f64::MAX).unwrap();
        let c = BigDecimal::from_f64(f64::MAX).unwrap();
        let d = BigDecimal::from_f64(f64::MAX).unwrap();
        assert_eq!(a.multiply(b).unwrap() / c, d);
    }

    #[test]
    fn check_div() {
        let a: BigDecimal = BigDecimal::from_f32(10.0).unwrap();
        let b: BigDecimal = BigDecimal::from_f32(10.0).unwrap();
        let c: BigDecimal = BigDecimal::from_f32(1.0).unwrap();
        assert_eq!(a.divide(b).unwrap(), c);

        let a = BigDecimal::from_f32(10.0).unwrap();
        let b = BigDecimal::from_f32(0.1).unwrap();
        let c = BigDecimal::from_f32(100.0).unwrap();
        assert_eq!(a.divide(b).unwrap(), c);

        let a = BigDecimal::from_f64(f64::MAX).unwrap();
        let b = BigDecimal::from_f64(0.1).unwrap();
        let c = BigDecimal::from_f64(f64::MAX).unwrap();
        let d = BigDecimal::from_f64(10.0).unwrap();
        assert_eq!(a.divide(b).unwrap(), c * d);

        let a = BigDecimal::from_f64(f64::MAX).unwrap();
        let b = BigDecimal::from_f64(f64::MAX).unwrap();
        let c = BigDecimal::from_f64(f64::MAX).unwrap();
        let d = BigDecimal::from_f64(f64::MAX).unwrap();
        assert_eq!(a.divide(b).unwrap() * c, d);
    }

    #[test]
    fn check_factorial() {
        let a: BigDecimal = BigDecimal::from_f32(6.0).unwrap();
        let b: BigDecimal = BigDecimal::from_f32(720.0).unwrap();
        assert_eq!(a.factorial().unwrap(), b);

        let a = BigDecimal::from_f32(10.0).unwrap();
        let b = BigDecimal::from_f32(3628800.0).unwrap();
        assert_eq!(a.factorial().unwrap(), b);

        let a = BigDecimal::from_f32(10.0).unwrap();
        let b = BigDecimal::from_f32(3628800.0).unwrap();
        assert_eq!(a.factorial().unwrap(), b);

        let a = BigDecimal::from_f32(2.0).unwrap();
        let b = BigDecimal::from_f32(2.0).unwrap();
        assert_eq!(a.factorial().unwrap(), b);

        let a = BigDecimal::from_f32(1.0).unwrap();
        let b = BigDecimal::from_f32(1.0).unwrap();
        assert_eq!(a.factorial().unwrap(), b);

        let a = BigDecimal::from_f32(0.0).unwrap();
        let b = BigDecimal::from_f32(1.0).unwrap();
        assert_eq!(a.factorial().unwrap(), b);

        let a = BigDecimal::from_f32(-1.0).unwrap();
        assert_eq!(a.factorial(), Err(CalcError::NegativeFactorial));
    }

    #[test]
    fn check_pow() {
        let a: BigDecimal = BigDecimal::from_f32(3.0).unwrap();
        let b: u32 = 3;
        let c: BigDecimal = BigDecimal::from_f32(27.0).unwrap();
        assert_eq!(a.pow(b).unwrap(), c);

        let a = BigDecimal::from_f64(f64::MAX).unwrap();
        let b = u32::MAX;
        assert_eq!(a.pow(b), Err(CalcError::Overflow));

        let a = BigDecimal::from_f32(10.0).unwrap();
        let b = 1;
        let c = BigDecimal::from_f32(10.0).unwrap();
        assert_eq!(a.pow(b).unwrap(), c);

        let a = BigDecimal::from_f32(9.98765).unwrap();
        let b = 0;
        let c = BigDecimal::from_f32(1.0).unwrap();
        assert_eq!(a.pow(b).unwrap(), c);

        let a = BigDecimal::from_f32(12.0).unwrap();
        let b = 5;
        let c = BigDecimal::from_f32(248832.0).unwrap();
        assert_eq!(a.pow(b).unwrap(), c);

        let a = BigDecimal::from_f32(0.1).unwrap();
        let b = 10;
        let c = BigDecimal::from_f32(1e-10).unwrap();
        assert_eq!(a.pow(b).unwrap(), c);

        let a = BigDecimal::from_f32(0.0).unwrap();
        let b = 2;
        let c = BigDecimal::from_f32(0.0).unwrap();
        assert_eq!(a.pow(b).unwrap(), c);

        let a = BigDecimal::from_f32(5.0).unwrap();
        let b = 13;
        let c = BigDecimal::from_i32(1_220_703_125).unwrap();
        assert_eq!(a.pow(b).unwrap(), c);
    }

    #[test]
    fn check_root() {
        let a: BigDecimal = BigDecimal::from_f32(4.0).unwrap();
        let b: u32 = 2;
        let c: BigDecimal = BigDecimal::from_f32(2.0).unwrap();
        assert_eq!(a.root(b).unwrap(), c);

        let a: BigDecimal = BigDecimal::from_f32(4.0).unwrap();
        let b: u32 = 0;
        assert_eq!(a.root(b), Err(CalcError::RootInvalidCombination));

        let a: BigDecimal = BigDecimal::from_f32(4.0).unwrap();
        let b: u32 = 1;
        let c: BigDecimal = BigDecimal::from_f32(4.0).unwrap();
        assert_eq!(a.root(b).unwrap(), c);

        let a: BigDecimal = BigDecimal::from_i64(254803968).unwrap();
        let b: u32 = 5;
        let c: BigDecimal = BigDecimal::from_f32(48.0).unwrap();
        assert_eq!(a.root(b).unwrap(), c);

        let a: BigDecimal = BigDecimal::from_i64(-9).unwrap();
        let b: u32 = 4;
        assert_eq!(a.root(b), Err(CalcError::RootInvalidCombination));

        let a: BigDecimal = BigDecimal::from_i64(-27).unwrap();
        let b: u32 = 3;
        let c: BigDecimal = BigDecimal::from_i32(-3).unwrap();
        assert_eq!(a.root(b).unwrap(), c);
    }
    #[test]
    fn check_super_factorial() {
        let a: BigDecimal = BigDecimal::from_f32(6.0).unwrap();
        let b: BigDecimal = BigDecimal::from_i32(24883200).unwrap();
        assert_eq!(a.super_factorial().unwrap(), b);

        let a = BigDecimal::from_f32(1.0).unwrap();
        let b = BigDecimal::from_f32(1.0).unwrap();
        assert_eq!(a.super_factorial().unwrap(), b);

        let a = BigDecimal::from_f32(2.0).unwrap();
        let b = BigDecimal::from_f32(2.0).unwrap();
        assert_eq!(a.super_factorial().unwrap(), b);

        let a = BigDecimal::from_f32(1.0).unwrap();
        let b = BigDecimal::from_f32(1.0).unwrap();
        assert_eq!(a.super_factorial().unwrap(), b);

        let a = BigDecimal::from_f32(0.0).unwrap();
        let b = BigDecimal::from_f32(1.0).unwrap();
        assert_eq!(a.super_factorial().unwrap(), b);

        let a = BigDecimal::from_f32(-1.0).unwrap();
        assert_eq!(a.super_factorial(), Err(CalcError::NegativeFactorial));

        let a = BigDecimal::from_u64(u64::MAX).unwrap();
        assert_eq!(a.super_factorial(), Err(CalcError::Overflow));
    }
    #[test]
    fn check_percentage(){
        let a = BigDecimal::from_f32(50.0).unwrap();
        let b = BigDecimal::from_f32(20.0).unwrap();
        let c = BigDecimal::from_f32(10.0).unwrap();
        assert_eq!(a.percentage(b).unwrap(), c);

        let a = BigDecimal::from_f32(0.0).unwrap();
        let b = BigDecimal::from_f32(20.0).unwrap();
        let c = BigDecimal::from_f32(0.0).unwrap();
        assert_eq!(a.percentage(b).unwrap(), c);

        let a = BigDecimal::from_f32(100.0).unwrap();
        let b = BigDecimal::from_f32(20.0).unwrap();
        let c = BigDecimal::from_f32(20.0).unwrap();
        assert_eq!(a.percentage(b).unwrap(), c);

        let a = BigDecimal::from_f32(200.0).unwrap();
        let b = BigDecimal::from_f32(20.0).unwrap();
        let c = BigDecimal::from_f32(40.0).unwrap();
        assert_eq!(a.percentage(b).unwrap(), c);

        let a = BigDecimal::from_f32(-100.0).unwrap();
        let b = BigDecimal::from_f32(20.0).unwrap();
        assert_eq!(a.percentage(b), Err(CalcError::InvalidPercentage));
    }
}

/// Calculation errors
#[derive(Debug, PartialEq)]
pub enum CalcError {
    /// result would overflow
    Overflow,
    /// result would underflow
    Underflow,
    /// attempted division by zero
    DivByZero,
    /// Invalid combination passed to root function (returned when root doesn't
    /// exist e.g. square root of -1)
    RootInvalidCombination,
    /// Returned when attempting to get root of negative number
    NegativeFactorial,
    /// Decimal factorial
    DecimalFactorial,
    /// Returned when calculating negative percentage of number
    InvalidPercentage,
}

/// Trait containing all the functions
pub trait LibMath<T>
where
    Self: Sized,
{
    ///Adds two numbers together
    fn add(&self, y: T) -> Result<Self, CalcError>;
    ///Subtracts numbers
    fn subtract(&self, y: T) -> Result<Self, CalcError>;
    ///Multiplies numbers
    fn multiply(&self, y: T) -> Result<Self, CalcError>;
    ///Divides numbers, returns [DivByZero](CalcError::DivByZero) when dividing by 0
    fn divide(&self, y: T) -> Result<Self, CalcError>;
    /// Returns factorial of self or [NegativeFactorial](CalcError::NegativeFactorial)
    /// when trying to get factorial of negative number or [Overflow](CalcError::Overflow)
    /// when calculating factorial of number larger than u64::max
    fn factorial(&self) -> Result<Self, CalcError>;
    /// Returns self raised to `exp`, returns [Overflow](CalcError::Overflow) when `exp > u32::max`
    fn pow(&self, exp: u32) -> Result<Self, CalcError>;
    /// returns `deg`-th root of self` or [RootInvalidCombination](CalcError::RootInvalidCombination)
    /// when result cannot be calculated (0 as degree, or even degree and negative
    /// self)
    fn root(&self, deg: u32) -> Result<Self, CalcError>;
    /// Returns superfactorial of itself, [NegativeFactorial](CalcError::NegativeFactorial)
    /// or [Overflow](CalcError::Overflow) when calculation would take unreasonably long
    fn super_factorial(&self) -> Result<Self, CalcError>;
    /// returns `self`% from y
    /// # Example
    /// ```
    /// use library::LibMath;
    /// use bigdecimal::BigDecimal;
    /// let i : BigDecimal = 25_i32.into();
    /// let j : BigDecimal = 8_i32.into();
    /// let k : BigDecimal = 2_i32.into();
    /// assert_eq!(i.percentage(j).unwrap(), k);
    ///
    /// ```
    fn percentage(&self, y: T) -> Result<Self, CalcError>;
}

trait LibMathPrivate<T>
where
    Self: Sized,
{
    fn newton_root(&self, deg: u32) -> Result<Self, CalcError>;
}

impl LibMath<BigDecimal> for BigDecimal {
    fn add(&self, y: BigDecimal) -> Result<Self, CalcError> {
        Ok(self + y)
    }

    fn subtract(&self, y: BigDecimal) -> Result<Self, CalcError> {
        Ok(self - y)
    }

    fn multiply(&self, y: BigDecimal) -> Result<Self, CalcError> {
        Ok(self * y)
    }

    fn divide(&self, y: BigDecimal) -> Result<Self, CalcError> {
        if y == BigDecimal::zero() {
            return Err(CalcError::DivByZero);
        }
        Ok(self / y)
    }

    fn factorial(&self) -> Result<Self, CalcError> {
        // Checks
        if self < &BigDecimal::zero() {
            return Err(CalcError::NegativeFactorial);
        }

        let loop_count = match self.to_u64() {
            Some(n) => n,
            None => return Err(CalcError::Overflow),
        };

        // Calculate
        let mut factorial = BigDecimal::one();
        for i in 1..=loop_count {
            factorial *= BigDecimal::from(i)
        }

        Ok(factorial)
    }

    fn pow(&self, exp: u32) -> Result<Self, CalcError> {
        // 1^anything = 1
        if self == &BigDecimal::one() {
            return Ok(self.clone());
        }

        match exp {
            0 => Ok(BigDecimal::one()),
            1 => Ok(self.clone()),
            2 => Ok(self.square()),
            3 => Ok(self.cube()),
            _ => {
                // This would take too long
                if exp > u16::MAX.into() {
                    return Err(CalcError::Overflow);
                }

                // Calculation method inspired by official implementation
                // of square() and cube() methods
                let (digits, scale) = self.as_bigint_and_exponent();
                Ok(BigDecimal::new(
                    digits.clone().pow(exp),
                    scale * exp.to_i64().unwrap(),
                ))
            }
        }
    }

    fn root(&self, deg: u32) -> Result<Self, CalcError> {
        match deg {
            0 => Err(CalcError::RootInvalidCombination),
            1 => Ok(self.clone()),
            2 => match self.sqrt() {
                Some(n) => Ok(n),
                None => Err(CalcError::RootInvalidCombination),
            },
            3 => Ok(self.cbrt()),
            _ => {
                if self < &BigDecimal::zero() && deg % 2 == 0 {
                    return Err(CalcError::RootInvalidCombination);
                }
                {
                    self.newton_root(deg)
                }
            }
        }
    }

    fn super_factorial(&self) -> Result<Self, CalcError> {
        if self < &BigDecimal::zero() {
            return Err(CalcError::NegativeFactorial);
        }
        if *self > BigDecimal::from_i32(i32::MAX).unwrap() {
            return Err(CalcError::Overflow);
        }

        let n = match self.to_u64() {
            Some(n) => n,
            None => return Err(CalcError::Overflow),
        };

        // Calculate
        let mut result = BigDecimal::one();
        for i in 1..=n {
            result *= BigDecimal::from_u64(i).unwrap().factorial()?;
        }

        Ok(result)
    }

    fn percentage(&self, y: BigDecimal) -> Result<Self, CalcError>{
        if *self < BigDecimal::zero(){
            return Err(CalcError::InvalidPercentage);
        }
        let base = self.divide(BigDecimal::from_i32(100).unwrap())?;
        base.multiply(y)
    }
}

impl LibMathPrivate<BigDecimal> for BigDecimal {
    fn newton_root(&self, deg: u32) -> Result<Self, CalcError> {
        if deg < 2 {
            return Err(CalcError::RootInvalidCombination);
        }

        let mut guess = self.clone();
        let mut guess_next = BigDecimal::one();
        let const1 = BigDecimal::from_f64((deg as f64 - 1.0) / deg as f64).unwrap();
        let const2 = self / deg;

        let epsilon = BigDecimal::from_f32(1e-10).unwrap();

        while (&guess - &guess_next).abs() >= epsilon {
            guess = guess_next;

            let guess_to_k_1 = match guess.pow(deg - 1) {
                Ok(n) => n,
                Err(e) => return Err(e),
            };

            guess_next = &const1 * &guess + &const2 / guess_to_k_1;
        }

        Ok(guess_next.with_scale(10))
    }
}
