Hneď po prvom stretnutí nám z tímu odišiel jeden človek. Museli sme nanovo
prerozdeliť prácu medzi ostatných.
Stanovený časový plán sa nám nepodarilo naplniť. Kým väčšina testov bola hotová
v stanovenom termíne, ostatné časti projektu sme dokončovali priebežne až do
deadline-u. Najprv sa tvorili testy a následne sa vykonávala implementácia
knižnice. Potom prišla na rad implementácia GUI a niekoľko refaktorizácií kódu.
Postupne sme prešli na GUI založené na udalostiach. Priebežne sme doplňovali
programovú dokumentáciu. Používateľská dokumentácia bola vytvorená až na konci.
Mockup sme tvorili v nástroji Figma. 

Rozdelenie podúloh sme plnili približne, ako bolo dohodnuté. Ak sa vyskytli
problémy a niektorý člen tímu niečo nevedel, pomáhali sme si navzájom.

Komunikácia prebiehala cez Discord a nebol s ňou problém. Všetci sme odpovedali
a riešili problémy priebežne.
Na bug tracking sme si zvolili Trello. Tento nástroj sa nám osvedčil a
priniesol do procesu opravy chýb väčší poriadok.
Samotný projekt sme tvorili s pomocou GITu. Využili sme privátny repozitár
na GitHube presne tak, ako bolo plánované.
