/*!
 * Calculator text output
 */

use bigdecimal::{BigDecimal, Zero, One, num_bigint::ToBigInt, FromPrimitive, ToPrimitive};
use copypasta::{ClipboardContext, ClipboardProvider};
use fltk::prelude::*;
use std::{str::FromStr, fmt};

use library::LibMath;

/// Operation function prototype
#[derive(Copy, Clone)]
pub enum OpFn {
    /// Prototype for function with one argument (e.g. factorial)
    OneArg(fn(&BigDecimal) -> Result<BigDecimal, library::CalcError>),
    /// Prototype for function with two arguments (e.g. plus)
    TwoArgs(fn(&BigDecimal, BigDecimal) -> Result<BigDecimal, library::CalcError>),
    /// Prototype for function with one `BigDecimal` and one `u32` argument (e.g. power)
    TwoArgsU32(fn(&BigDecimal, u32) -> Result<BigDecimal, library::CalcError>)
}

/// Pending operation
///
/// Used to track, which math operation should be invoked when enter (or =) is
/// pressed.
pub struct PendingOp {
    function: OpFn,
}

/// Text output
///
/// Now it's basically a wrapper around fltk::frame::Frame, but this will very
/// likely change.
pub struct TextOutput {
    frame: fltk::frame::Frame,
    content: String,
    clipboard: copypasta::ClipboardContext,
    input: ReadingBuff,
}

/// Struct holding input (1 or 2 numbers and operator)
struct ReadingBuff{
    result_printed: bool,
    /// BigDecimal - number, u64 - last decimal position
    left: (BigDecimal, u64),
    right: Option<(BigDecimal, u64)>,
    operator: Option<PendingOp>,
}

impl fmt::Debug for ReadingBuff{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ReadingBuff: {{left: {:?}, right: {:?}, operator: {:?}}}",
               self.left, self.right, self.operator.is_some())
    }
}


impl ReadingBuff{
    /// initialiye reading buff
    fn clear(&mut self) {
        self.left.0 = BigDecimal::zero();
        self.left.1 = 0;
        self.right = None;
        self.result_printed = false;
        self.operator = None;
    }
    fn new() -> Self{
        ReadingBuff { result_printed: false,
                      left: (BigDecimal::zero(), 0),
                      right: None,
                      operator: None }
    }
}

impl TextOutput {
    /// Returns new `TextOutput` from `fltk::frame::Frame`
    pub fn from(frame: fltk::frame::Frame) -> TextOutput {
        TextOutput {
            frame,
            content: "".to_string(),
            clipboard: ClipboardContext::new().unwrap(),
            input: ReadingBuff::new(),
        }
    }

    /// Appends string to the text output
    pub fn append(&mut self, s: &str) {
        //get current number buffer left before typing operator, right after it
        // was entered
        if self.input.result_printed{
            self.clear();
        }
        if let Some(PendingOp{function}) = self.input.operator{
            if let OpFn::OneArg(_) = function{
                return;
            }
        }
        let num : & mut (BigDecimal, u64) = if self.input.operator.is_none(){
            &mut self.input.left
              }else{
            match &mut self.input.right{
                Some(n) => n,
                None =>{
                    self.input.right = Some((BigDecimal::zero(), 0));
                    self.input.right.as_mut().unwrap()
                }
            }
        };

        if self.input.result_printed{
            num.1 = 0;
            num.0 = BigDecimal::zero();
            self.input.result_printed = false;
        }

        //insert decimal point
        if s == "."{
            if num.1 == 0{
                num.1 = 1;
                self.content.push_str(s);
                self.display();
            }
            return;

        }
        if num.1 == 0{ //check if decimal point exists, add to the end
            num.0 *= BigDecimal::from_i16(10).unwrap();
            num.0 += BigDecimal::from_str(s).unwrap();
        }else{
            let exp = num.1;
            let addition = BigDecimal::from_str(s).unwrap() /
                BigDecimal::from_i16(10).unwrap().pow(exp as u32).unwrap();
            num.0 += addition;
            num.1 += 1;
        }

        self.content.push_str(s);
        self.display();
    }

    /// Convert `self.content` to scientific notation
    fn convert_scientific_output(num: BigDecimal) -> Result<String, ()>{
        let num_abs = num.abs();
        let str = num_abs.to_string();
        //large numbers
        if num_abs > BigDecimal::one(){
            let vec: Vec<&str> = str.split(".").collect();//split while number and decimal, parts
            if vec.len() == 0{//empty input nothing to do
                return Ok("".to_string());
            }
            let exp = vec[0].len() - 1;
            let mut base = vec[0].to_string();
            base.insert(1, '.');//insert decimal point
            if vec.len() == 2{//insert decimal part of number if it exists
                base.push_str(vec[1]);
            }
            if num < BigDecimal::zero(){
                base.insert(0, '-');
            }
            if base.len() > 21{
                let _unused = base.split_off(20);
            }
            loop{
                if base.ends_with('0') || base.ends_with('.'){
                    base.remove(base.len() - 1);
                }else{
                    break;
                }
            }
            return Ok(format!("{}e{}", base, exp));
        }else{//very small numbers
            if let Ok(inv) = BigDecimal::one().divide(num_abs.clone()){
                let exp = inv.to_bigint().ok_or(())?.to_string().len() as u32;
                let vec: Vec<&str> = str.split(".").collect();//split while number and decimal, parts
                if vec.len() == 0{//nothing to do
                    return Ok("".to_string());
                }
                let mut base = vec[1].to_string();//copy decimal place
                base.drain(..(exp - 1) as usize);//remove 0s from beginning
                if base.len() > 1{
                    base.insert(1, '.');//insert decimal dot
                }
                if num < BigDecimal::zero(){
                    base.insert(0, '-');
                }
                if base.len() > 21{
                    let _unused = base.split_off(20);
                }
                loop{
                    if base.ends_with('0') || base.ends_with('.'){
                        base.remove(base.len() - 1);
                    }else{
                        break;
                    }
                }
                return Ok(format!("{}e-{}", base, exp));

            }
        }
        return Err(());//bad luck
    }

    /// Displays resulting number
    pub fn display(&mut self){
        self.frame.set_label(&self.content);
    }

    /// Clears text output
    pub fn clear(&mut self) {
        self.input.clear();
        self.content.clear();
        self.display();
        self.clear_pending_op();

    }

    /// Deletes last letter from the input
    pub fn backspace(&mut self) {
        //remove from input buffer
        let num : & mut (BigDecimal, u64) = if self.input.operator.is_none(){
            &mut self.input.left
              }else{
            match &mut self.input.right{
                Some(n) => n,
                None =>{
                    self.input.operator = None;
                    self.content.pop();
                    if self.content.ends_with("s") {
                        self.content.pop();
                    }
                    self.display();
                    return;
                }
            }

        };
        if self.input.result_printed{
            num.1 = 0;
            num.0 = BigDecimal::zero();
        }

        let mut as_str = num.0.to_string();
        if as_str.pop() == Some('.'){
            as_str.pop();
        }
        num.0 = BigDecimal::from_str(&as_str).unwrap_or(BigDecimal::zero());
        if num.1 > 0{
            num.1 -= 1;
        }
        //remove from backspace
        if !self.content.is_empty(){
            self.content.pop();
        }
        if self.input.result_printed{
            self.content.clear();
        }

        self.display();

    }

    /// Copies contents of text output
    pub fn copy(&mut self){
        self.clipboard.set_contents(self.frame.label()).unwrap();
    }

    /// Sets pending operation (+, -, *,...)
    pub fn set_pending_op(&mut self, symbol: &'static str, function: OpFn) {
        //if operator exists, execute it
        if self.input.result_printed{
            self.clear();
        }
        if self.input.operator.is_some(){
            self.invoke_pending_op();
        }
        if self.input.right.is_none() && self.input.operator.is_some(){
            self.content.pop();
        }

        //set next operator
        self.input.operator = Some(PendingOp {
            function,
        });
        self.input.right = None;

        self.content.push_str(symbol);
        self.display();
    }

    /// Removes pending operation
    fn clear_pending_op(&mut self) {
        self.input.operator = None;
    }

    fn convert_result_to_output_str(&mut self, num: &BigDecimal) {
            const UPPER_LIMIT: u64= 1_000_000_000;
            const LOWER_LIMIT: f64= 0.000_000_001;
            if num.abs() > BigDecimal::from_u64(UPPER_LIMIT).unwrap() ||
                num.abs() < BigDecimal::from_f64(LOWER_LIMIT).unwrap() &&
                num.abs() != BigDecimal::zero()
        {
            if let Ok(s) = Self::convert_scientific_output(num.clone()){
                self.content = s;
            }else{
                self.frame.set_label("Error");
            }
        } else {
            let mut out = format!("{:.20}", num);
            if out.len() > 21{
                let _unused = out.split_off(20);
            }
            while out.ends_with('0') || out.ends_with('.'){
                if out.ends_with('.'){
                    out.remove(out.len() - 1);
                    break;
                }
                out.remove(out.len() - 1);
            }
            self.content = out;
        }
    }

    pub fn print_result(&mut self){
        self.invoke_pending_op();
        self.input.result_printed = true;
        self.display();
    }

    /// Invokes current pending operation, if there's any and displays result
    /// to the user.
    pub fn invoke_pending_op(&mut self) {
        if let Some(pending_op) = &self.input.operator {
            // Run operation
            let result = match pending_op.function {
                OpFn::OneArg(f) => {
                    f(&self.input.left.0)
                },
                OpFn::TwoArgs(f) => {
                    match &self.input.right{
                        Some(rhs) => f(&self.input.left.0, rhs.0.clone()),
                        None => return,
                    }
                },
                OpFn::TwoArgsU32(f) => {
                    match &self.input.right{
                        Some(rhs) =>
                        {
                            let y: u32 = match rhs.0.to_u32(){
                                Some(n)  => n,
                                None => {
                                    self.content.clear();
                                    self.frame.set_label("Error");
                                    return;
                                }
                            };
                            f(&self.input.left.0, y)
                        },
                        None => return,
                    }
                }
            };

            // Display result
            match result {
                Ok(value) => {
                    // Display calculation value
                    self.content = value.to_string();
                    self.input.left.0 = value.clone();
                    self.convert_result_to_output_str(&value);
                    self.display();
                    self.input.operator = None;
//                    self.input.result_printed = false;
                },
                Err(_) => {
                    // Invalid calculation
                    self.content.clear();
                    self.frame.set_label("Error");
                    self.content = "Error".to_string();
                    self.input.clear();
                }
            }
            self.input.operator = None;
        }
    }
}
