use bigdecimal::{BigDecimal, Zero, One, FromPrimitive};
use library::LibMath;
use std::io;
use std::io::Read;
use std::str::FromStr;

/// Reads input from stdin and returns a string
fn read_stdin() -> String {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input).unwrap();
    input
}

/// Parses input string and returns vector of `BigDecimal`s
fn parse_input(input: &str) -> Vec<BigDecimal> {
    let mut result = Vec::new();
    for token in input.split_whitespace() {
        let token = token.trim();
        if token.is_empty() {
            continue;
        }
        let token = BigDecimal::from_str(token).unwrap();
        result.push(token);
    }
    result
}

/// Calculates arithmetic mean of vector of `BigDecimal`s
fn arithmetic_mean(input: &Vec<BigDecimal>) -> BigDecimal {
    let mut sum = BigDecimal::zero();
    for token in input {
        sum = token.add(sum).unwrap();
    }
    sum.divide(BigDecimal::from_i32(input.len() as i32).unwrap()).unwrap()
}

/// Calculates sampling standard deviation of vector of `BigDecimal`s
fn sampling_standard_deviation(values: &Vec<BigDecimal>) -> BigDecimal {
    let mean = arithmetic_mean(values);
    let n = BigDecimal::from_i32(values.len() as i32).unwrap();
    let mut sum = BigDecimal::zero();

    for value in values {
        sum = sum.add(value.pow(2).unwrap()).unwrap();
    }

    let diff = mean.pow(2).unwrap().multiply(BigDecimal::from_i32(values.len() as i32).unwrap()).unwrap();
    let sum = sum.subtract(diff).unwrap();
    let variance = sum.divide(n.subtract(BigDecimal::one()).unwrap()).unwrap();

    variance.root(2).unwrap()
}

fn main() {
    let input = read_stdin();
    let parsed = parse_input(&input);
    println!("{:.10}", sampling_standard_deviation(&parsed));
}
