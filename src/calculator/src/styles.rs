/*!
 * Styles for the calculator
 */

use fltk::{
    enums::{Color}
};

/// Size of a single button in pixels
pub const BUTTON_SIZE: i32 = 50;

/// Calculator keyboard offset from top left (x, y)
pub const KEYBOARD_OFFSET: (i32, i32) = (0, 50);

/// Keyboard size
pub const KEYBOARD_SIZE: (i32, i32) = (5, 5);

/// Button color specification
#[derive(Copy, Clone, Debug)]
pub struct ButtonColor {
    /// Color of button in it's default state (not pressed or hovered over)
    pub default: Color,
    /// Button color when hovered over
    pub hover: Color,
    /// Color when pressed
    pub press: Color
}

/// Color of number buttons
pub const BTN_COLOR_NUM: ButtonColor = ButtonColor {
    default: Color::from_hex(0x0A9396),
    hover: Color::from_hex(0x087D7F),
    press: Color::from_hex(0x00333F)
};

/// Color of function buttons
pub const BTN_COLOR_FN: ButtonColor = ButtonColor {
    default: Color::from_hex(0xDFD500),
    hover: Color::from_hex(0xC7BE00),
    press: Color::from_hex(0x444100)
};

/// Color of function buttons
pub const BTN_COLOR_CLR_DEL: ButtonColor = ButtonColor {
    default: Color::from_hex(0xB85500),
    hover: Color::from_hex(0x9E4A00),
    press: Color::from_hex(0x4D2400)
};
