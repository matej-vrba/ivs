/*!
 * Calculator GUI
 */

use fltk::{
    enums::{Align, Color, Event, FrameType, Shortcut},
    prelude::*, image::PngImage
};
use std::path::Path;

use crate::styles;
use crate::output::TextOutput;

/// Event message
///
/// When, for example, button is pressed, it emits `Message::BtnPressed`, we
/// receive it and process it correspondingly.
#[derive(Debug, Copy, Clone)]
pub enum Message<'a> {
    /// Button pressed event
    ///
    /// Contains button's text as a value.
    BtnPressed(&'a str)
}

/// Calculator's GUI "class"
pub struct GUI<'a> {
    /// Application
    app: fltk::app::App,
    /// Application's window
    window: fltk::window::Window,
    /// Width of calculator's window
    window_width: i32,
    /// Frame to show user typed-in equation as a string
    text_output: TextOutput,

    /// Messages sender
    msg_sender: fltk::app::Sender<Message<'a>>,
    /// Messages receiver
    msg_receiver: fltk::app::Receiver<Message<'a>>
}

impl GUI<'static> {
    /// Return's new GUI instance including `app`, `window` and `text_output` frame.
    ///
    /// # Arguments
    /// * `window_title`
    /// * `screen_position` - position of created window from top left corner
    ///   of the screen
    pub fn new(window_title: &'static str, screen_position: (i32, i32)) -> GUI {
        // Create app
        let app = fltk::app::App::default();

        // Create window
        let window_width = styles::KEYBOARD_SIZE.0 * styles::BUTTON_SIZE
            + styles::KEYBOARD_OFFSET.0;
        let window_height = styles::KEYBOARD_SIZE.1 * styles::BUTTON_SIZE
            + styles::KEYBOARD_OFFSET.1;
        let mut window = fltk::window::Window::new(
            screen_position.0,
            screen_position.1,
            window_width,
            window_height,
            window_title
        );

        if let Ok(icon) = PngImage::load(Path::new("/usr/share/pixmaps/kalkulacka.png")){
            window.set_icon(Some(icon));
        }
        // Create frame to display calculation (equation) as string
        let mut frame = fltk::frame::Frame::default()
            .with_size(window_width, styles::KEYBOARD_OFFSET.1)
            .with_label("");
        frame.set_color(Color::from_hex(0xabcdef)); // TODO
        frame.set_frame(FrameType::FlatBox);
        frame.set_align(Align::Right | Align::Inside);

        // Button operation channel
        let (msg_sender, msg_receiver) = fltk::app::channel::<Message>();

        GUI {
            app,
            window,
            window_width,
            text_output: TextOutput::from(frame),
            msg_sender,
            msg_receiver
        }
    }

    /// Ends the window.
    ///
    /// Should be called when all widgets are added and ready.
    pub fn end(&mut self) {
        self.window.make_resizable(false);
        self.window.end();
        self.window.show();
    }

    /// Registers handler to receive app events
    pub fn receive_messages(&mut self, f: fn(Message, &mut TextOutput)) {
        while self.app.wait() {
            if let Some(msg) = self.msg_receiver.recv() {
                f(msg, &mut self.text_output);
            }
        }
    }

    /// Adds nth button row
    pub fn add_btn_row(&self, nth_row: i32) -> fltk::group::Pack {
        fltk::group::Pack::new(
            styles::KEYBOARD_OFFSET.0,
            styles::KEYBOARD_OFFSET.1 + nth_row * styles::BUTTON_SIZE,
            self.window_width,
            styles::BUTTON_SIZE,
            ""
        )
    }

    /// Ends (nth) button row
    pub fn end_btn_row(mut added_btn_row: fltk::group::Pack) {
        added_btn_row.end();
        added_btn_row.set_type(fltk::group::PackType::Horizontal);
    }

    /// Adds a button.
    ///
    /// # Arguments
    ///
    /// * `text` - Text shown inside the button
    /// * `color` - Color struct
    /// * `shortcut` - Physical keyboard shortcut for this button.
    ///
    /// This is meant to be used with `add_btn_row()` and
    /// `end_btn_row()` functions.
    pub fn add_btn(
        &mut self,
        text: &'static str,
        color: styles::ButtonColor,
        shortcut: Shortcut
    ) -> fltk::button::Button{
        let mut b = fltk::button::Button::new(
            0,
            0,
            styles::BUTTON_SIZE,
            styles::BUTTON_SIZE,
            text
        );

        // Styling
        b.set_frame(FrameType::FlatBox);
        b.set_label_color(Color::Black);
        b.set_color(color.default);
        b.set_selection_color(color.press);
        b.handle(move |btn, e| match e {
            Event::Enter => {
                btn.set_color(color.hover);
                btn.redraw();
                true
            }
            Event::Leave => {
                btn.set_color(color.default);
                btn.redraw();
                true
            }
            _ => false
        });

        // Emit button pressed message (event)
        b.emit(self.msg_sender, Message::BtnPressed(text));

        // Add keyboard alias
        b.set_shortcut(shortcut);
        return b;
    }
}
