extern crate copypasta;

use fltk::{
    enums::{Key, Shortcut},
    prelude::WidgetExt
};
use library;
use library::LibMath;
use bigdecimal::BigDecimal;

mod output;
mod gui;
mod styles;

fn main() {
    // Initialize GUI
    let mut gui = gui::GUI::new("Kalkulačka", (0, 100));

    // Add buttons
    let btn_row = gui.add_btn_row(0);
    gui.add_btn("DEL", styles::BTN_COLOR_CLR_DEL, Shortcut::None | Key::BackSpace).set_tooltip("Vymazať posledný znak poľa");
    gui.add_btn("CE", styles::BTN_COLOR_CLR_DEL, Shortcut::None | Key::Delete).set_tooltip("Vymazať pole");
    gui.add_btn("Cpy", styles::BTN_COLOR_FN, Shortcut::Ctrl | Key::from_char('c')).set_tooltip("Skopírovať text");
    gui.add_btn("×", styles::BTN_COLOR_FN, Shortcut::None | Key::from_char('*')).set_tooltip("Násobenie");
    gui.add_btn("%", styles::BTN_COLOR_FN, Shortcut::None | Key::from_char('%')).set_tooltip("Percent z");
    gui::GUI::end_btn_row(btn_row);

    let btn_row = gui.add_btn_row(1);
    gui.add_btn("1", styles::BTN_COLOR_NUM, Shortcut::None | Key::from_char('1'));
    gui.add_btn("2", styles::BTN_COLOR_NUM, Shortcut::None | Key::from_char('2'));
    gui.add_btn("3", styles::BTN_COLOR_NUM, Shortcut::None | Key::from_char('3'));
    gui.add_btn("/", styles::BTN_COLOR_FN, Shortcut::None | Key::from_char('/')).set_tooltip("Delenie");
    gui.add_btn("√", styles::BTN_COLOR_FN, Shortcut::None | Key::from_char('r')).set_tooltip("Odmocnina");
    gui::GUI::end_btn_row(btn_row);

    let btn_row = gui.add_btn_row(2);
    gui.add_btn("4", styles::BTN_COLOR_NUM, Shortcut::None | Key::from_char('4'));
    gui.add_btn("5", styles::BTN_COLOR_NUM, Shortcut::None | Key::from_char('5'));
    gui.add_btn("6", styles::BTN_COLOR_NUM, Shortcut::None | Key::from_char('6'));
    gui.add_btn("-", styles::BTN_COLOR_FN, Shortcut::None | Key::from_char('-')).set_tooltip("Odčítanie");
    gui.add_btn("s!", styles::BTN_COLOR_FN, Shortcut::Ctrl | Key::from_char('!')).set_tooltip("Superfaktoriál");
    gui::GUI::end_btn_row(btn_row);

    let btn_row = gui.add_btn_row(3);
    gui.add_btn("7", styles::BTN_COLOR_NUM, Shortcut::None | Key::from_char('7'));
    gui.add_btn("8", styles::BTN_COLOR_NUM, Shortcut::None | Key::from_char('8'));
    gui.add_btn("9", styles::BTN_COLOR_NUM, Shortcut::None | Key::from_char('9'));
    gui.add_btn("+", styles::BTN_COLOR_FN, Shortcut::None | Key::from_char('+')).set_tooltip("Sčítanie");
    gui.add_btn("!", styles::BTN_COLOR_FN, Shortcut::None | Key::from_char('!')).set_tooltip("Faktoriál");
    gui::GUI::end_btn_row(btn_row);

    let btn_row = gui.add_btn_row(4);
    gui.add_btn("0", styles::BTN_COLOR_NUM, Shortcut::None | Key::from_char('0'));
    gui.add_btn("00", styles::BTN_COLOR_NUM, Shortcut::Ctrl | Key::from_char('0'));
    gui.add_btn(".", styles::BTN_COLOR_NUM, Shortcut::None | Key::from_char('.'));
    gui.add_btn("=", styles::BTN_COLOR_FN, Shortcut::None | Key::Enter);
    gui.add_btn("^", styles::BTN_COLOR_FN, Shortcut::None | Key::from_char('^')).set_tooltip("Umocnenie");
    gui::GUI::end_btn_row(btn_row);

    gui.end();
    gui.receive_messages(|msg, text_output| match msg {
        gui::Message::BtnPressed(text) => match text {

            "DEL" => text_output.backspace(),
            "CE" => text_output.clear(),
            "+" => text_output.set_pending_op("+", output::OpFn::TwoArgs(BigDecimal::add)),
            "-" => text_output.set_pending_op("-", output::OpFn::TwoArgs(BigDecimal::subtract)),
            "×" => text_output.set_pending_op("×", output::OpFn::TwoArgs(BigDecimal::multiply)),
            "/" => text_output.set_pending_op("/", output::OpFn::TwoArgs(BigDecimal::divide)),
            "%" => text_output.set_pending_op("%", output::OpFn::TwoArgs(BigDecimal::percentage)),
            "^" => text_output.set_pending_op("^", output::OpFn::TwoArgsU32(BigDecimal::pow)),
            "!" => text_output.set_pending_op("!", output::OpFn::OneArg(BigDecimal::factorial)),
            "s!" => text_output.set_pending_op("s!", output::OpFn::OneArg(BigDecimal::super_factorial)),
            "√" => text_output.set_pending_op("√", output::OpFn::TwoArgsU32(BigDecimal::root)),
            "=" => text_output.print_result(),
            "Cpy" => text_output.copy(),
            "00" => {
                text_output.append("0");
                text_output.append("0");
            },
            _ => text_output.append(text),

        }
    });
}
